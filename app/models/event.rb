class Event < ActiveRecord::Base
	acts_as_commontable
	mount_uploader :picture, PictureUploader
	belongs_to :user
end
