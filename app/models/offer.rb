class Offer < ActiveRecord::Base
	mount_uploader :picture, PictureUploader
	validates :title, :cost, :localization, presence: true
	belongs_to :user
end
