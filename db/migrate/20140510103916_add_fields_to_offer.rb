class AddFieldsToOffer < ActiveRecord::Migration
  def change
  	add_column :offers, :on_sale, :boolean
  	add_column :offers, :status, :boolean
  end
end
