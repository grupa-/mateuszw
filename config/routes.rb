FlatRent::Application.routes.draw do
  resources :offers

  resources :events

  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users

  mount Commontator::Engine => '/commontator'
end